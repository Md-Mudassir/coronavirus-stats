# Coronavirus-Stats

Coronavirus (COVID-19) around the world statistics visualized.

Developed using React, Material-UI, Chart.js

API: https://covid19.mathdro.id/api

Explore: https://coronayrus.netlify.com/

Credits: @adrianhajdin