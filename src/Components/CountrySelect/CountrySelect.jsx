import React, { useState, useEffect } from "react";
import styles from "./CountrySelect.module.css";

import { fetchCountries } from "../../API";

const CountrySelect = ({ handleCountryChange }) => {
  const [fetchedCountries, setFetchedCountries] = useState([]);

  useEffect(() => {
    const fetchAPI = async () => {
      setFetchedCountries(await fetchCountries());
    };
    fetchAPI();
  }, [setFetchedCountries]);

  return (
    <form>
      <input
        type="text"
        name="country"
        list="country-datalist"
        className={styles.inputstyle}
        defaultValue=""
        placeholder="Enter Country"
        onSelect={(e) => handleCountryChange(e.target.value)}
        inputMode="text"
      />
      <datalist id="country-datalist">
        {fetchedCountries.map((country, i) => (
          <option key={i} value={country}>
            {country}
          </option>
        ))}
      </datalist>
    </form>
  );
};

export default CountrySelect;
